const store = (key, value) => {
  if (!global.__store) {
    global.__store = {}
  }
  global.__store[key] = value
  return true
}

const getStored = (key) => {
  if (global.__store && global.__store[key]) {
    return global.__store[key]
  } else {
    return null
  }
}

module.exports = {
  store,
  getStored
}
