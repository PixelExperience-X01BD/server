const fs = require('fs')
const util = require('util')

const getFiles = (directoryPath) => {
  return new Promise((resolve, reject) => {
    fs.readdir(directoryPath, (err, files) => {
      if (err) {
        reject(err)
      }
      resolve(files)
    })
  })
}

const readFile = util.promisify(fs.readFile)
const writeFile = util.promisify(fs.writeFile)

module.exports = {
  getFiles,
  readFile,
  writeFile
}
