const parseDate = date => {
  const formattedDate = [
    date.slice(0, 4),
    date.slice(4, 6),
    date.slice(6, 8)
  ].join('-')
  return new Date(Date.parse(formattedDate))
}

const getDateTime = () => {
  const today = new Date()
  var date = [today.getFullYear(), today.getMonth(), today.getDate()].join('-')
  const time = [today.getHours(), today.getMinutes(), today.getSeconds()].join(':')
  return `${date} ${time}`
}

module.exports = {
  parseDate,
  getDateTime
}
