import ListComponent from './list.svelte'

const builds = new ListComponent({
  target: document.querySelector('.builds-list'),
  props: {
    // we'll learn about props later
    answer: 42
  }
})
console.log(builds.length)
