require('dotenv').config()
const ProjectsModule = require('./modules/projects')
const ServerModule = require('./modules/server')
const FrontendModule = require('./modules/frontend')

const locationPath = __dirname.split('/')
locationPath.pop()
const location = locationPath.join('/')

let firmwareFolder
if (process.argv.length > 2 &&
    process.argv[2] === 'dockerized') {
  firmwareFolder = '/var/www/firmwares/'
} else {
  firmwareFolder = process.env.FIRMWARE_FOLDER
}

global.config = {
  port: process.env.PORT,
  location,
  firmwareFolder
}
const projects = new ProjectsModule()
const frontend = new FrontendModule(projects)
const server = new ServerModule(frontend)

server.start()
