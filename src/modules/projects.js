
const fs = require('fs')

const Module = require('../prototypes/module')
const {
  getFiles,
  readFile
} = require('../helpers/fs')
const { parseDate } = require('../helpers/date')

class ProjectsModule extends Module {
  constructor () {
    super()
    this.buildProjects()
      .then(projects => (this._projects = projects))
      .then(() => this.log(`Inited with ${this._projects.length} firmwares`))
  }

  get list () {
    return this._projects
  }

  watch () {
    fs.watch(global.config.firmwareFolder, async () => {
      this._projects = await this.buildProjects()
      this.log('File change detected')
      this.emit('update', {})
    })
  }

  getAbsolutePath (fileName) {
    return `${global.config.firmwareFolder}/${fileName}`
  }

  async generateBuildInfo (changelogPath) {
    const zipPath = changelogPath.replace('-Changelog.txt', '.zip')
    const nameItems = changelogPath.split('-')
    this.log(`Fetching build info for ${nameItems[3]}`)
    return {
      buildNumber: nameItems[3],
      date: parseDate(nameItems[2]),
      changeLog: (await readFile(
        this.getAbsolutePath(changelogPath)
      )).toString(),
      file: fs.existsSync(
        this.getAbsolutePath(zipPath)
      )
        ? zipPath : null
    }
  }

  async buildProjects () {
    this.log('Building firmware list...')
    const builds = []
    const files = await getFiles(global.config.firmwareFolder)
    for (const fileName of files) {
      if (fileName.endsWith('-Changelog.txt')) {
        builds.push(await this.generateBuildInfo(fileName))
      }
    }
    builds.sort((a, b) => {
      const dateA = new Date(a.date)
      const dateB = new Date(b.date)
      return dateB - dateA
    })
    this.log('Firmware list builded')
    return builds
  }
}

module.exports = ProjectsModule
