const express = require('express')
const Module = require('../prototypes/module')
// const { readFile } = require('../helpers/fs')

class ServerModule extends Module {
  constructor (frontend) {
    super()
    this.app = express()
    this.frontend = frontend
    this.log(`Inited`)
  }

  start () {
    this.app.get('/projects', (req, res, next) => {
      res.setHeader('Content-Type', 'application/json')
      res.end(this.frontend.getJsonBuilds())
    })

    this.app.use('/', express.static(`${global.config.location}/dist/`))

    this.app.get('/', (req, res, next) => {
      const locale = req.headers['accept-language']
        .split(';')[0]
        .split(',')[0]
        .split('-')[0]
      res.sendFile(`${global.config.location}/dist/index.${locale}.html`)
    })
    this.app.use('/downloads', express.static(global.config.firmwareFolder))
    this.app.listen(global.config.port)
    this.log(`Started on ${global.config.port}`)
  }
}

module.exports = ServerModule
