const Module = require('../prototypes/module')
const showdown = require('showdown')
const nunjucks = require('nunjucks')
const postcss = require('postcss')
const locales = require('../helpers/i18n')

const rollup = require('rollup')
const svelte = require('rollup-plugin-svelte')
const resolve = require('rollup-plugin-node-resolve')
const { terser } = require('rollup-plugin-terser')

const renderer = nunjucks.configure('src/frontend', {
  autoescape: true
})
const {
  readFile,
  writeFile
} = require('../helpers/fs')

const postcssPlugins = [
  require('postcss-import'),
  require('postcss-preset-env'),
  require('cssnano')
]

renderer.addFilter('i18n', function (val) {
  const locale = locales[this.ctx.lang]
  if (locale) {
    for (const phrase of locale) {
      if (val === phrase[0]) return phrase[1]
    }
  }
  return val
})

class FrontendModule extends Module {
  constructor (projects) {
    super()
    this.converter = new showdown.Converter()
    this.projects = projects
    this.projects.on('update', () => {
      this.renderTemplates()
    })
    this.projects.watch()
    this.langs = ['en', 'ru']
    this.buildCSS()
    this.buildJS()
    this.renderTemplates()
    this.log('Inited')
  }

  async buildCSS () {
    this.log('Building CSS...')
    const inFile = 'src/frontend/css/styles.css'
    const outFile = 'dist/styles.css'
    const css = (await readFile(inFile)).toString()
    return postcss(postcssPlugins).process(css, {
      from: inFile,
      to: outFile
    })
      .then(result => writeFile(outFile, result))
      .then(() => this.log('CSS builded'))
  }

  async buildJS () {
    this.log('Building JS...')
    const outputOptions = {
      file: 'dist/app.js',
      format: 'iife'
    }
    const bundle = await rollup.rollup({
      input: 'src/frontend/main.js',
      plugins: [
        svelte({
          include: 'src/frontend/*.svelte'
        }),
        resolve(),
        terser()
      ]
    })
    await bundle.generate(outputOptions)
    await bundle.write(outputOptions)
    this.log('JS builded')
  }

  renderTemplates () {
    this.log('Rendering templates…')
    for (const lang of this.langs) {
      writeFile(
        `dist/index.${lang}.html`,
        renderer.render('index.html', {
          buildsString: 'Сборки',
          lang
        })
      )
    }
    this.log(`Templates rendered for ${this.langs.length} locales`)
  }

  getJsonBuilds () {
    this.log(`Rendering JSON...`)
    const builds = this.projects.list
    builds.map((build) => {
      build.changeLog = this.converter.makeHtml(build.changeLog)
      return build
    })
    this.log(`JSON rendered`)
    return JSON.stringify(builds)
  }
}

module.exports = FrontendModule
