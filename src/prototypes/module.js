const colors = require('colors/safe')
const EventEmitter = require('events')

class Module {
  constructor () {
    this.emitter = new EventEmitter()
  }

  emit (eventName) {
    this.emitter.emit(eventName)
  }

  addListener (eventName, callback) {
    this.emitter.addListener(eventName, callback)
  }

  on (eventName, callback) {
    this.emitter.on(eventName, callback)
  }

  log () {
    console.log(
      colors.gray(Date.now() / 1000 | 0),
      colors.yellow(this._name),
      ...arguments
    )
  }

  get _name () {
    if (!Module._maxNameLength) {
      Module._maxNameLength = 0
    }
    let name = `${this.constructor.name}:`
    const lengthDiff = Module._maxNameLength - this.constructor.name.length
    if (lengthDiff < 0) {
      Module._maxNameLength = this.constructor.name.length
    } else if (lengthDiff > 0) {
      for (let i = 0; i < lengthDiff; i++) {
        name += ' '
      }
    }
    return name
  }
}

module.exports = Module
